# FROM node:20-alpine AS deps
FROM alpine
# RUN apk add --no-cache libc6-compat
# RUN apk add --update python3 make g++ && rm -rf /var/cache/apk/*
# WORKDIR /app
# COPY package.json yarn.lock ./
# RUN npm install

# FROM node:20-alpine AS builder
# WORKDIR /app
# COPY --from=deps /app/node_modules ./node_modules
# COPY . .

# RUN yarn build

# FROM node:20-alpine AS runner
# WORKDIR /app

# ENV NODE_ENV production
# # Uncomment the following line in case you want to disable telemetry during runtime.
# # ENV NEXT_TELEMETRY_DISABLED 1

ARG MODULE
ENV module_env=$MODULE

# RUN addgroup --system --gid 1001 nodejs
# RUN adduser --system --uid 1001 nextjs

RUN echo $module_env
RUN echo "this is from the dockerfile"

# You only need to copy next.config.js if you are NOT using the default configuration
# COPY --from=builder /app/next.config.js ./
# COPY --from=builder /app/public ./public
# COPY --from=builder /app/package.json ./package.json

# COPY --from=builder --chown=nextjs:nodejs /app/.next/standalone ./
# COPY --from=builder --chown=nextjs:nodejs /app/.next/static ./.next/static

# USER nextjs

# EXPOSE 3000

# ENV PORT 3000

# CMD ["node", "server.js"]

